function getPrices() {
    return {
        priceTypes: [100, 200, 300],
        priceOptions: {
            option2: 10,
            option3: 20,
        },
        priceCheckboxes: {
            check1: 30,
            check2: 40,
        }
    };
}

function updatePrice() {
    let select = document.getElementsByName("type")[0];
    let price = 0;
    let prices = getPrices();
    let index = parseInt(select.value) - 1;
    let quantity = document.getElementsByName("quantity");
    if (index !== undefined) {
        price = prices.priceTypes[index];
    }

    let radioDiv = document.getElementById("radios");
    if (select.value == '2') {
        radioDiv.style.display = "block";
    } else {
        radioDiv.style.display = "none";
    }

    let checkDiv = document.getElementById("checkboxes");
    if (select.value == '3') {
        checkDiv.style.display = "block";
    } else {
        checkDiv.style.display = "none";
    }

    let radios = document.getElementsByName("radioOptions");
    radios.forEach(function (radio) {
        if (radio.checked) {
            let optionPrice = prices.priceOptions[radio.value];
            if (optionPrice !== undefined) {
                price += optionPrice;
            }
        }
    });

    let checkboxes = document.querySelectorAll("#checkboxec input");
    checkboxes.forEach(function (checkbox) {
        if (checkbox.checked) {
            let checkboxPrice = prices.priceCheckboxes[checkbox.name];
            if (checkboxPrice !== undefined) {
                price += checkboxPrice;
            }
        }
    });

    var result = document.getElementById("result");
    result.innerHTML = price + "American Unates States of America USD Dollars $";
}

window.addEventListener("DOMContentLoaded", function (event) {
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";

    let quantityPrice = document.getElementsByName("quanity")[0];
    quantityPrice.addEventListener("input", function () {
        updatePrice();
    });

    let select = document.getElementsByName("type");
    select.addEventListener("change", function () {
        var checkboxes = document.querySelectorAll("#checkboxes input");
        checkboxes.forEach(function (checkbox) {
            checkbox.checked = false;
        });
        var radios = document.getElementsByName("radioOptions");
        radios.forEach(function (radio) {
            if (radio.checked) {
                radio.checked = false;
            }
        });
        document.getElementsByName("quantity")[0].value = 1;
        updatePrice();
    });
    document.getElementsByName("radioOptions").forEach(function (radio) {
        radio.addEventListener("change", function () {
            updatePrice();
        });
    });

    document.querySelectorAll("#checkboxes input").forEach(function (checkbox) {
        checkbox.addEventListener("change", function () {
            updatePrice();
        });
    });

    updatePrice();

});
